from datetime import datetime
from django.db import models
from django.contrib.auth.models import User

SHORT_TEXT_LEN = 1000


class Article(models.Model):
    title = models.CharField(max_length=200)
    release_date = models.DateTimeField(blank=True,null=False)
    text = models.TextField()
    image = models.ImageField(upload_to='')
    user = models.ForeignKey(User)

    def __str__(self):
        return self.title

    def get_short_text(self):
        if len(self.text) > SHORT_TEXT_LEN:
            return self.text[:SHORT_TEXT_LEN]
        else:
            return self.text

    class Meta:
        ordering=['-release_date']